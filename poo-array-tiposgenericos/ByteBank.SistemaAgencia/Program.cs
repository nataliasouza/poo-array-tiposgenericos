﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ByteBank.Modelos;
using ByteBank.Modelos.Funcionarios;

namespace ByteBank.SistemaAgencia
{
    class Program
    {
        static void Main(string[] args)
        {
            ListaDeContaCorrente lista = new ListaDeContaCorrente();
            lista.MeuMetodo(numero: 10);
            lista.MeuMetodo("texto padrao", 10);

            ContaCorrente contaDaNat = new ContaCorrente(789, 596412385);
            lista.Adicionar(contaDaNat);

            lista.Adicionar(new ContaCorrente(874, 5769687));
            lista.Adicionar(new ContaCorrente(874, 5566778));
            lista.Adicionar(new ContaCorrente(874, 5557778));
            lista.Adicionar(new ContaCorrente(874, 5544778));
            lista.Adicionar(new ContaCorrente(874, 5552278));
            lista.Adicionar(new ContaCorrente(874, 5568996));
            lista.Adicionar(new ContaCorrente(874, 5552248));

            lista.EscreverListaNaTela();
            lista.Remover(contaDaNat);

            Console.ReadLine();
        }
        static void TestaArrayDeContaCorrente()
        {
            ContaCorrente[] contas = new ContaCorrente[]
            {
                new ContaCorrente(874, 5767887),
                new ContaCorrente(874, 4456668),
                new ContaCorrente(874, 8899776)
            };

            for (int indice = 0; indice < contas.Length; indice++)
            {
                ContaCorrente contaAtual = contas[indice];
                Console.WriteLine($"Conta {indice} {contaAtual.Numero}");
            }
        }
        static void TestaArrayInt()
        {
            // ARRAY de inteiros, com 5 posições!
            int[] idades = new int[6];

            idades[0] = 15;
            idades[1] = 28;
            idades[2] = 35;
            idades[3] = 50;
            idades[4] = 28;
            idades[5] = 60;

            Console.WriteLine(idades.Length);

            int acumulador = 0;
            for (int indice = 0; indice < idades.Length; indice++)
            {
                int idade = idades[indice];
                Console.WriteLine($"Acessando o array idades no índice {indice}");
                Console.WriteLine($"Valor de idades[{indice}] = {idade}");

                acumulador += idade;
            }

            int media = acumulador / idades.Length;
            Console.WriteLine($"Média de idades = {media}");

            Console.WriteLine();
        }
    }
}
